﻿var Input = {
    init: function () {
        Search();
    },
}

function Create() {
    var Url = "../Input/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../Input/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id, AccountId, ClientDeposit, TransActionCase) {
    var Url = "../Input/Delete?Id=" + id + "&AccountId=" + AccountId + "&ClientDeposit=" + ClientDeposit + "&TransActionCase=" + TransActionCase;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id, AccountId, ClientDeposit, TransActionCase) {
    var Url = "../Input/DeleteRow?Id=" + id + "&AccountId=" + AccountId + "&ClientDeposit=" + ClientDeposit + "&TransActionCase=" + TransActionCase;;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageTasksAnalysis/Index";
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#Name').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../Input/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


