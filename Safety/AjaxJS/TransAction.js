﻿var TransAction = {
    init: function () {
        Search();
    },
}

function Create() {
    var Url = "../TransAction/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../TransAction/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id, FromAccountID, ToAccountID, Amount) {
    var Url = "../TransAction/Delete?Id=" + id + "&FromAccountID=" + FromAccountID + "&ToAccountID=" + ToAccountID + "&Amount=" + Amount;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id, FromAccountID, ToAccountID, Amount) {
    var Url = "../TransAction/DeleteRow?Id=" + id + "&FromAccountID=" + FromAccountID + "&ToAccountID=" + ToAccountID + "&Amount=" + Amount;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageTasksAnalysis/Index";
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#Name').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../TransAction/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


