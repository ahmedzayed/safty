﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Safety.Core;
using Safety.Models;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Safety.Controllers
{
    public class RoleGroupController : Controller
    {
        
        // GET: RoleGroup
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllRole".ExecuParamsSqlOrStored(false).AsList<RoleVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            RoleVM Obj = new RoleVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(RoleVM model)
        {
            ApplicationDbContext cont = new ApplicationDbContext();
            var rolemanger = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(cont));
            var LastId = "SP_GetAllRole".ExecuParamsSqlOrStored(false).AsList<RoleVM>().Count ;
            model.Id =(Convert.ToInt32( LastId) + 1).ToString();

            var role = new IdentityRole();
            role.Name = model.Name;
            rolemanger.Create(role);
                
           


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");




        }


        //public ActionResult Edit(int id = 0)
        //{

        //    var model = "SP_SelectRoleById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<RoleVM>();
        //    if (model == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("Create", model.FirstOrDefault());
        //}



        [HttpGet]
        public ActionResult Delete(string id)
        {
            RoleVM obj = new RoleVM();
            obj.Id = id;
            return PartialView("~/Views/RoleGroup/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(string Id)
        {
            var data = "SP_DeleteRole".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }


        public ActionResult UserRole()
        {
          

            UserRoleVM model = new UserRoleVM();

            return View(model);
        }

        public ActionResult UserRoleSearch()
        {

            var model = "SP_GetAllUserRole".ExecuParamsSqlOrStored(false).AsList<UserRoleVM>();
            return View(model);
        }



       public ActionResult AddUserRole()
        {
            var UserDrop = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            UserDrop.AddRange("Sp_UserDrop".ExecuParamsSqlOrStored(false).AsList<UserRoleDrop>().Select(s => new SelectListItem
            {
                Text = s.UserName,
                Value = s.Id.ToString(),
                Selected = s.Id == "0" ? true : false
            }).ToList());
            ViewBag.UserLList = UserDrop;
            var RoleDrop = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            RoleDrop.AddRange("Sp_RoleDrop".ExecuParamsSqlOrStored(false).AsList<RoleDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == "0" ? true : false
            }).ToList());
            ViewBag.RoleList = RoleDrop;
            UserRoleVM model = new UserRoleVM();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddUserRole(UserRoleVM model)
        {
            var model1 = "SP_GetAllUserRoleById".ExecuParamsSqlOrStored(false,"UserId".KVP(model.UserId)).AsList<UserRoleVM>();
            if(model1.Count > 0)
            {
                TempData["success"] = "<script>alert(' تم ربط هذا المستخدم بصلاحية من قبل ');</script>";
                return RedirectToAction("UserRole");

            }

            var data = "SP_AddUseRole".ExecuParamsSqlOrStored(false, "UserId".KVP(model.UserId), "RoleId".KVP(model.RoleId)).AsNonQuery();

            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("UserRole");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("UserRole");
        }



    }
}