﻿using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    [Authorize(Roles = "Admin,Client")]

    public class ClientController : Controller
    {
        // GET: Client
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllClient".ExecuParamsSqlOrStored(false).AsList<ClientVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            ClientVM Obj = new ClientVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(ClientVM model)
        {
            model.StartDate = DateTime.Now;
            if (Request.Files.Count > 0)
            {


              
                if ( Request.Files != null)
                {
                   
                    var file = Request.Files[0];
                    
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.IDPhoto1 = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                    var file2 = Request.Files[1];
                  
                    var fileNameWithExtension2 = Path.GetFileName(file2.FileName);
                    var fileNameOnly2 = Path.GetFileNameWithoutExtension(file2.FileName);
                    string RootPath2= Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique2 = new Guid();
                    var path2 = Path.Combine(RootPath2, fileNameOnly2 + unique2 + Path.GetExtension(file2.FileName));
                    file2.SaveAs(path2);
                    model.IDPhoto2 = "Images/" + fileNameOnly2 + unique2 + Path.GetExtension(file2.FileName);


                }




            }

            var data = "SP_AddClient".ExecuParamsSqlOrStored(false, "Address".KVP(model.Address), "Email".KVP(model.Email), "FullName".KVP(model.FullName)
                , "IDPhoto1".KVP(model.IDPhoto1), "IDPhoto2".KVP(model.IDPhoto2), "IsActive".KVP(model.IsActive), "Mobile".KVP(model.Mobile), "Note".KVP(model.Note), "StartDate".KVP(model.StartDate), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectClient".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ClientVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            ClientVM obj = new ClientVM();
            obj.ID = id;
            return PartialView("~/Views/Client/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteClient".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}