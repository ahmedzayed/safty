﻿using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    [Authorize(Roles = "Admin,User")]

    public class InPutController : Controller
    {
        // GET: InPut
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>();
            return View(model);

        }


        [HttpPost]
        public ActionResult GetAccount(int id=0)
        {

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false,"Id".KVP(id)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            return Json(Accounts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            InPutFM Obj = new InPutFM();
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var Accounts= new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false,"Id".KVP(0)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            var model = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>().LastOrDefault();
            if (model == null)
            {
                Obj.TransNumber = "01";
            }
            else
            {
                int Oldnu = Convert.ToInt32(model.TransNumber);
                int newval = Oldnu + 1;
               
                Obj.TransNumber = "0"+ newval.ToString();
            }

            return View(Obj);

        }
        [HttpPost]
        public ActionResult Create(InPutFM model)
        {

          

            model.TransDate = DateTime.Now;
            model.ClientDeposit = model.ClientDeposit - 10;

            var data = "SP_AddInput".ExecuParamsSqlOrStored(false, "AccountID".KVP(model.AccountID), "ClientDeposit".KVP(model.ClientDeposit), "Fee".KVP(model.Fee), "Note".KVP(model.Note), "TransDate".KVP(model.TransDate), "TransNumber".KVP(model.TransNumber), "TransActionCase".KVP(1), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountTypeDrop".ExecuParamsSqlOrStored(false).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            var model = "SP_SelectInputById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<InPutFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id, int AccountId, decimal ClientDeposit, int TransActionCase)
        {
            InPutFM obj = new InPutFM();
            obj.Id = id;
            obj.AccountID = AccountId;
            obj.ClientDeposit = ClientDeposit;
            obj.TransActionCase = TransActionCase;
            return PartialView("~/Views/InPut/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id, int AccountId, decimal ClientDeposit, int TransActionCase)
        {
            var data = "SP_DeleteInPut".ExecuParamsSqlOrStored(false, "Id".KVP(Id), "AccountId".KVP(AccountId), "ClientDeposit".KVP(ClientDeposit), "TransActionCase".KVP(TransActionCase)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}