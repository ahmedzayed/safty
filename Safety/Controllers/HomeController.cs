﻿using Microsoft.AspNet.Identity;
using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Safety.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
           
            ViewBag.DepositeCount = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>().Count;
            ViewBag.WithdrawCount = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>().Count;
            ViewBag.TransactionCount = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>().Count;
            ViewBag.AllCounts = ViewBag.DepositeCount + ViewBag.WithdrawCount+ ViewBag.TransactionCount;
            if (ViewBag.DepositeCount != 0)
            {
                ViewBag.DepositeCountPrec = (ViewBag.AllCounts / ViewBag.DepositeCount) * 100;
            }
            if (ViewBag.WithdrawCountPrec != null)
            {


                ViewBag.WithdrawCountPrec = (ViewBag.AllCounts / ViewBag.WithdrawCount) * 100;
            }
            if (ViewBag.TransactionCountPrec != null)
            {
                ViewBag.TransactionCountPrec = (ViewBag.AllCounts / ViewBag.TransactionCount) * 100;
            }

            //ViewBag.WithdrawVol = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>().Where(a => a.TransDate >= DateTime.Now.AddDays(-10)).Count();
            //ViewBag.MaxVol = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>().Count();
            //ViewBag.WithdrawTotalVol= "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>().Sum(a=>a.ClientDeposit);


            //ViewBag.DepositeVol = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>().Where(a => a.TransDate >= DateTime.Now.AddDays(-10)).Count();
            //ViewBag.DepositeTotalVol = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>().Sum(a => a.ClientDeposit);


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult LastFiveAction()
        {
            var model = "SP_GetAllInPutReport".ExecuParamsSqlOrStored(false).AsList<InPutVM>();

            return PartialView("_LastFiveAction");

        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}