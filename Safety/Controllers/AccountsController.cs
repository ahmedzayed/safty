﻿using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllAccounts".ExecuParamsSqlOrStored(false).AsList<AccountsVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            AccountsFM Obj = new AccountsFM();
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID== 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var AccountType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            AccountType.AddRange("Sp_AccountTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountTypeList = AccountType;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(AccountsFM model)
        {

            model.StartDate = DateTime.Now;

            var data = "SP_AddAccounts".ExecuParamsSqlOrStored(false, "AccountNo".KVP(model.AccountNo), "AccountTypeID".KVP(model.AccountTypeID), "ClientID".KVP(model.ClientID), "CurrentAmount".KVP(model.CurrentAmount), "IsActive".KVP(model.IsActive), "StartDate".KVP(model.StartDate), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var AccountType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            AccountType.AddRange("Sp_AccountTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountTypeList = AccountType;

            var model = "SP_SelectAccountsById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AccountsFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            AccountsFM obj = new AccountsFM();
            obj.ID = id;
            return PartialView("~/Views/Accounts/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteAccounts".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}