﻿using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    public class TransActionController : Controller
    {
        [Authorize(Roles = "Admin,User")]

        // GET: TransAction
        public ActionResult Index1()
        {

            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>();
            return View(model);

        }


        [HttpPost]
        public ActionResult GetAccount(int id = 0)
        {

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            return Json(Accounts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            TransActionFM Obj = new TransActionFM();
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false, "Id".KVP(0)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(TransActionFM model)
        {

            var CurrentAmount = "SP_GetCurrentAmount".ExecuParamsSqlOrStored(false, "AccountID".KVP(model.FromAccountID)).AsList<AccountsFM>().FirstOrDefault();
            if (CurrentAmount.CurrentAmount < model.Amount)
            {
                TempData["success"] = "<script>alert(' الرصيد الحالى اقل من الرصيد المطلوب للتحويل');</script>";
                return RedirectToAction("Index1");
            }

            model.TransDate = DateTime.Now;
            

            var data = "SP_AddTransAction".ExecuParamsSqlOrStored(false, "Amount".KVP(model.Amount), 
                "Fee".KVP(model.Fee), "FromAccountID".KVP(model.FromAccountID), "Note".KVP(model.Note),
                "ToAccountID".KVP(model.ToAccountID), "TransCode".KVP(model.TransCode), "TransDate".KVP(model.TransDate), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {
            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountTypeDrop".ExecuParamsSqlOrStored(false).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            var model = "SP_SelectTransActionById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<TransActionFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id,int FromAccountID,int ToAccountID, decimal Amount)
        {
            TransActionFM obj = new TransActionFM();
            obj.ID= id;
            obj.FromAccountID = FromAccountID;
            obj.ToAccountID = ToAccountID;
            obj.Amount = Amount;
           
            return PartialView("~/Views/TransAction/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id, int FromAccountID, int ToAccountID, decimal Amount)
        {
            var data = "SP_DeleteTransAction".ExecuParamsSqlOrStored(false, "Id".KVP(Id), "FromAccountID".KVP(FromAccountID), "ToAccountID".KVP(ToAccountID),"Amount".KVP(Amount)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}