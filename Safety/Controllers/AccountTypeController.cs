﻿using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AccountTypeController : Controller
    {
        // GET: AccountType
        public ActionResult Index1()
        {
            
            return View();
        }

        public ActionResult Search()
        {
         
                var model = "SP_GetAllAccountType".ExecuParamsSqlOrStored(false).AsList<AccountTypeVM>();
                return View(model);
           
        }


        public ActionResult Create()
        {
            AccountTypeVM Obj = new AccountTypeVM();

            return View(Obj);
        }
        [HttpPost]
        public ActionResult Create(AccountTypeVM model)
        {

           
            var data = "SP_AddAccountType".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "Description".KVP(model.Description), "IsDeleted".KVP(false), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {


                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                return RedirectToAction("Index1");

            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

            return RedirectToAction("Index1");

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectAccountTypeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AccountTypeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            AccountTypeVM obj = new AccountTypeVM();
            obj.ID = id;
            return PartialView("~/Views/AccountType/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteAccountType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}