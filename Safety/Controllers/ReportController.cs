﻿using CrystalDecisions.CrystalReports.Engine;
using Safety.Core;
using Safety.VM.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Safety.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult BankIncome()
        {
            return View();
        }
        public ActionResult BankIncomeSearch()
        {
            BankingVM model = new BankingVM();
            var Input = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>();
            model.Count = Input.Count();
            model.Fee = Input.Sum(a => a.Fee);
            var OutPut = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>();
            model.Count += OutPut.Count;
            model.Fee += OutPut.Sum(a => a.Fee);

            var TransAction = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>();
            model.Count += TransAction.Count;
            model.Fee += TransAction.Sum(a => a.Fee);


            return View(model);
        }


        public ActionResult DeptAccounts()
        {

            return View();
        }
        public ActionResult DeptAccountsSearch(DateTime? StartDate, DateTime? EndDate)
        {

            var model = "SP_GetAllAccounts".ExecuParamsSqlOrStored(false).AsList<AccountsVM>();
            if (StartDate != null && EndDate != null)
            {
                var model2 = model.Where(a => a.StartDate >= StartDate && a.StartDate <=EndDate).ToList();
                return View(model2);
            }
            else

                return View(model);
        }

        public ActionResult TransActionDetails()
        {
            return View();
        }
        public ActionResult TransActionDetailsSearch(DateTime? StartDate, DateTime? EndDate)
        {

            var model = "SP_GetAllInPutReport".ExecuParamsSqlOrStored(false).AsList<InPutVM>();

            if (StartDate != null && EndDate != null)
            {
                var model2 = model.Where(a => a.TransDate >= StartDate && a.TransDate <= EndDate).ToList();
                return View(model2.ToList());
            }
            else
                return View(model);
        }

        public ActionResult TransActionReport()
        {
            return View();
        }
        public ActionResult TransActionReportSearch(DateTime? StartDate, DateTime? EndDate)
        {
            var model = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>();
            if (StartDate != null && EndDate != null)
            {
                var model2 = model.Where(a => a.TransDate >= StartDate && a.TransDate <= EndDate).ToList();
                return View(model2);
            }
            else


                return View(model);
        }


        [HttpPost]
        public ActionResult GetAccount(int id = 0)
        {

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;

            return Json(Accounts, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AccountStatement()
        {

            var Cllst = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Cllst.AddRange("Sp_ClientDrop".ExecuParamsSqlOrStored(false).AsList<ClientDrop>().Select(s => new SelectListItem
            {
                Text = s.FullName,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.ClientList = Cllst;

            var Accounts = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Accounts.AddRange("Sp_AccountDrop".ExecuParamsSqlOrStored(false, "Id".KVP(0)).AsList<AccountDrop>().Select(s => new SelectListItem
            {
                Text = s.AccountNo.ToString(),
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.AccountList = Accounts;
            InPutFM model = new InPutFM();
            return View(model);
        }
        public ActionResult AccountStatementSearch(int? AccountId, DateTime? StartDate, DateTime? EndDate)
        {
            if (AccountId == 0 || StartDate == null || EndDate == null)
            {
                return View();
            }
            else
            {
                var model = "SP_GetAllAccountsReport".ExecuParamsSqlOrStored(false, "AccountId".KVP(AccountId)).AsList<AccountsVM>().FirstOrDefault();
                ViewBag.AccountType = model.AccountTypeName;
                ViewBag.ClientName = model.ClientName;
                ViewBag.CurrentAmount = model.CurrentAmount;

                var model1 = "SP_GetAllInPutReport".ExecuParamsSqlOrStored(false).AsList<InPutVM>();
                return View(model1.Where(a => a.TransDate >= StartDate && a.TransDate <= EndDate && a.AccountID == AccountId));

            }


        }



        #region Crystal
        public ActionResult AccountStatementReport(DateTime? StartDate, DateTime? EndDate)
        {
            ReportDocument rd = new ReportDocument();

            var model = "SP_GetAllAccounts".ExecuParamsSqlOrStored(false).AsList<AccountsVM>();
            var model2 = model.Where(a => a.StartDate >= StartDate && a.StartDate <= EndDate).ToList();
            if (StartDate != null && EndDate != null)
            {

                var model3 = model2.Select(p => new
                {
                    ID = p.ID,

                    AccountTypeName = p.AccountTypeName,
                    ClientName = p.ClientName,
                    CurrentAmount = p.CurrentAmount


                }).ToList();


                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/DeptAccounts.rpt")));
                rd.SetDataSource(model3);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "DeptAccounts.pdf");
                }
                catch
                {
                    throw;
                }

            }
            else
            {
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/DeptAccounts.rpt")));
                rd.SetDataSource(model);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "DeptAccounts.pdf");
                }
                catch
                {
                    throw;
                }
            }



        }


        public ActionResult BankIncomeReport(DateTime? StartDate, DateTime? EndDate)
        {
            ReportDocument rd = new ReportDocument();


            BankingVM model = new BankingVM();
            var Input = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(1)).AsList<InPutVM>();
            model.Count = Input.Count();
            model.Fee = Input.Sum(a => a.Fee);
            var OutPut = "SP_GetAllInPut".ExecuParamsSqlOrStored(false, "TransActionCase".KVP(2)).AsList<InPutVM>();
            model.Count += OutPut.Count;
            model.Fee += OutPut.Sum(a => a.Fee);

            var TransAction = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>();
            model.Count += TransAction.Count;
            model.Fee += TransAction.Sum(a => a.Fee);


            List<BankingVM> SS = new List<BankingVM>();
            SS.Add(model);
              



                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/BankIncome.rpt")));
                rd.SetDataSource(SS.ToList());

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "BankIncome.pdf");
                }
                catch
                {
                    throw;
                }

           
            



        }

        public ActionResult TransActionDetailsReport(DateTime? StartDate, DateTime? EndDate)
        {
            ReportDocument rd = new ReportDocument();

            var model = "SP_GetAllInPutReport".ExecuParamsSqlOrStored(false).AsList<InPutVM>();

            if (StartDate != null && EndDate != null)
            {
                var model2 = model.Where(a => a.TransDate >= StartDate && a.TransDate <= EndDate).ToList();

                model2.Select(a => new
                {
                    TransNumber = a.TransNumber,
                    TransActionCase = a.TransActionCase,
                    ClientName = a.ClientName,
                    AccountName = a.AccountName,
                    TransDate = a.TransDate,
                    ClientDeposit = a.ClientDeposit

                });

                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/TransActionDetails.rpt")));
                rd.SetDataSource(model2.ToList());

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "TransActionDetails.pdf");
                }
                catch
                {
                    throw;
                }


            }
            else
            {
                
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/TransActionDetails.rpt")));
                rd.SetDataSource(model.ToList());

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "TransActionDetails.pdf");
                }
                catch
                {
                    throw;
                }
            }



        }
        public ActionResult TransActionReportCr(DateTime? StartDate, DateTime? EndDate)
        {
            ReportDocument rd = new ReportDocument();

            var model = "SP_GetAllTransAction".ExecuParamsSqlOrStored(false).AsList<TransActionVM>();
           

            if (StartDate != null && EndDate != null)
            {
                var model2 = model.Where(a => a.TransDate >= StartDate && a.TransDate <= EndDate).ToList();

                model2.Select(a => new
                {
                    TransCode = a.TransCode,
                    ClientFromName = a.ClientFromName,
                    FromAccountName = a.FromAccountName,
                    ClienttoName = a.ClienttoName,
                    ToAccountName = a.ToAccountName,
                    TransDate = a.TransDate,
                    Amount = a.Amount

                });

                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/TransActionReport.rpt")));
                rd.SetDataSource(model2.ToList());

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "TransActionReport.pdf");
                }
                catch
                {
                    throw;
                }


            }
            else
            {

                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/TransActionDetails.rpt")));
                rd.SetDataSource(model.ToList());

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                try
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "TransActionDetails.pdf");
                }
                catch
                {
                    throw;
                }
            }



        }
        #endregion

    }
}