﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
  public  class InPutVM
    {
        public int Id { get; set; }
        public string TransNumber { get; set; }
        public DateTime TransDate { get; set; }
        public int AccountName { get; set; }
        public decimal ClientDeposit { get; set; }
        public string ClientName { get; set; }
        public int AccountID { get; set; }
        public decimal Fee { get; set; }
        public int TransActionCase { get; set; }
        public string Note { get; set; }
    }

}
