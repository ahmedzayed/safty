﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
 public   class ClientVM
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public string IDPhoto1 { get; set; }
        public string IDPhoto2 { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
       
    }
}
