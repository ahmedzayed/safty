﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
   public class AccountsVM
    {
        public int ID { get; set; }
        public int AccountNo { get; set; }
        public string AccountTypeName { get; set; }
        public decimal CurrentAmount { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsActive { get; set; }
        public string ClientName { get; set; }
    }
}
