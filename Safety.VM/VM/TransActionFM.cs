﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
   public class TransActionFM
    {
        public int ID { get; set; }
        public string TransCode { get; set; }
        public DateTime TransDate { get; set; }
        public int FromAccountID { get; set; }
        public int ClientFromID { get; set; }
        public int ToAccountID { get; set; }
        public int ClienttoID { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string Note { get; set; }
    }
}
