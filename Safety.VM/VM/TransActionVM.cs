﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
  public  class TransActionVM
    {
        public int ID { get; set; }
        public string TransCode { get; set; }
        public DateTime TransDate { get; set; }
        public int FromAccountName { get; set; }
        public string ClientFromName { get; set; }
        public int ToAccountName { get; set; }
        public string ClienttoName { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string Note { get; set; }
    }
}
