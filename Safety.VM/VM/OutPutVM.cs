﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safety.VM.VM
{
   public class OutPutVM
    {
        public int Id { get; set; }
        public string TransNumber { get; set; }
        public DateTime TransDate { get; set; }
        public int AccountName { get; set; }
        public decimal ClientWithdraw { get; set; }
        public string ClientName { get; set; }
        public int AccountID { get; set; }
        public decimal Fee { get; set; }
        public string Note { get; set; }
    }
}
